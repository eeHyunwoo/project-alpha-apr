from django.urls import path
from projects.views import (
    list_projects_view,
    list_project_details,
    create_project,
)

urlpatterns = [
    path("", list_projects_view, name="list_projects"),
    path("<int:id>", list_project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
